extends Area2D

export(float) var SPEED = 1500
export(float) var DAMAGE = 15

var direction = 0

func _ready():
	pass

func _process(delta):
	position.x += direction * SPEED * delta

func _on_Bullet_body_entered(body):
	if "Enemy" in body.name:
		body.dead()
		queue_free()
