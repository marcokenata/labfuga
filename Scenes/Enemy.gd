extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const GRAVITY = 10
const SPEED = 100
const FLOOR = Vector2(0,-1)

var velocity = Vector2()

var direction = 1

var is_dead = false

func dead():
	is_dead = true
	velocity = Vector2(0,0)
	$CollisionShape2D.disabled = true
	global.enemies -= 1
	queue_free()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	var collision = move_and_collide(delta * velocity)
	if collision:
		if collision.collider.has_method("respawn"):
			collision.collider.respawn()

	if is_dead == false:
		velocity.x = SPEED * direction
		$AnimatedSprite.play("walk")
		velocity.y += GRAVITY
		velocity = move_and_slide(velocity, FLOOR)
	
	if is_on_wall():
		direction = direction * -1
		
		
		
		
		
		
