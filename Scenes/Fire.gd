extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var animator = self.get_node("AnimationPlayer")
# Called when the node enters the scene tree for the first time.
func _physics_process(delta):
	animator.play("FireAnimation")

func _on_Fire_body_entered(body):
	if "orang" in body.name :
		body.respawn()
		queue_free()