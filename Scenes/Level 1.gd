extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var PAUSE = false
# Called when the node enters the scene tree for the first time.
const Bullet = preload("res://Scenes/Explosion.tscn")
const YouLost = preload("res://Scenes/You Lose.tscn")

func _ready():
	$Node2D.visible = false
	$"Node2D-e2".visible = false
	$"Node2D-e3".visible = false
	$"Node2D-e4".visible = false
	$"Node2D-e5".visible = false
	$Timer.start()
	
func _process(delta):
	$CanvasLayer/MarginContainer/label.set_text("Time left :"+str(int($Timer.time_left))+"\nWeapon left :"+str(global.senjata)+"\nChallenge : "+global.objectives+"\nRemaining Enemies : "+str(global.enemies))
	if global.enemies == 0:
		global.objectives = "Cleared!"

func _on_Timer_timeout():
	$Node2D.visible = true
	$"Node2D-e2".visible = true
	$"Node2D-e3".visible = true
	$"Node2D-e4".visible = true
	$"Node2D-e5".visible = true
	$Timer.stop()
	$Pintu/CollisionShape2D.disabled = true
	$Timer2.start()

func _on_Timer2_timeout():
	get_tree().change_scene_to(YouLost)
