extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("orang")
const Bullet = preload("res://Scenes/Bullet.tscn")
export (String) var sceneName = "Level 1"

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
	if !is_on_floor() and Input.is_action_just_pressed("jump"):
		velocity.y += jump_speed
	if Input.is_action_pressed("attack") and is_on_floor() and $Timer.is_stopped():
		if global.senjata > 0:
			_shoot() 
			global.senjata -= 1
			$Timer.start()
		else :
			pass

func respawn():
	position.x = position.x - 200
	position.y = position.y - 20

func _shoot():
	var bullet = Bullet.instance()
	get_parent().add_child(bullet)
	bullet.global_position = global_position
	bullet.direction = 1 if sprite.flip_h else -1

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		animator.play("jump")
	elif velocity.x != 0:
		animator.play("run")
		if velocity.x > 0:
			sprite.flip_h = true
		else:
			sprite.flip_h = false
	elif Input.is_action_pressed("attack") and is_on_floor() and global.senjata > 0:
		animator.play("attack")
	else:
		animator.play("New Anim")

func _on_Timer_timeout():
	$Timer.stop()
	

